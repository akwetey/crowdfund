<?php 
$settings = App\Models\AdminSettings::first(); 
$total_raised_funds = App\Models\Donations::sum('donation');
$total_campaigns    = App\Models\Campaigns::where('status','active')->count();
$total_members      = App\Models\User::count();
?>
@extends('app')

@section('content')
<div class="fadein">
	<div class ="img"><img src="http://farm3.static.flickr.com/2610/4148988872_990b6da667.jpg" width = "1500px" height= "450px"class="img-fluid">
	<h1 class="title-site txt-left" id="titleSite">A Little help, for a giant cause!</h1>
</div>
<div class = "img"><img src="http://farm3.static.flickr.com/2597/4121218611_040cd7b3f2.jpg" width = "1500px" height= "450px" class="img-fluid"><h1>A Little help, for a giant cause!</h1></div>
	<div class ="img" ><img src="http://farm3.static.flickr.com/2531/4121218751_ac8bf49d5d.jpg"width = "1500px" height= "450px" class="img-fluid"><h1>A Little help, for a giant cause!</h1><p class="subtitle-site txt-left"><strong>>Mary recently raised  GH₵6000 to fund her surgery</p></strong></div>
           
	
</div>

                               


@if( $categories->count() != 0 )
<div class="container margin-bottom-40">
	
			<div class="col-md-12 btn-block margin-bottom-40 head-home">
				<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{trans('misc.categories')}}</h1>
				<h5 class="btn-block text-center class-montserrat subtitle-color">
					<a href="{{ url('categories') }}">
			        			<strong>{{ trans('misc.view_all') }} <i class="fa fa-long-arrow-right"></i></strong>
			        		</a>
			        		</h5>
			</div>	
			 
			 @foreach(  App\Models\Categories::where('mode','on')->orderBy('name')->take(4)->get() as $category )	
			     
			     @include('includes.categories-listing')
			     
			  @endforeach

			 
</div><!-- container -->
@endif

@if( $data->total() != 0 )
	<div class="container margin-bottom-40">
			<div class="col-md-12 btn-block margin-bottom-40">
				<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{trans('misc.campaigns')}}</h1>
				<h5 class="btn-block text-center class-montserrat subtitle-color">{{trans('misc.recent_campaigns')}}</h5>
			</div>			
		
		<div class="margin-bottom-30">
			@include('includes.campaigns')
		</div>
		
		<div class="row margin-bottom-40">
		
		<div class="container">
			<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow"><span class=".numbers-with-commas counter"><?php echo html_entity_decode( App\Helper::formatNumbersStats($total_members) ) ?></span></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.members')}}</h5>
				</div><!-- col-md-3 -->
				
			<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow"><span class=".numbers-with-commas counter"><?php echo html_entity_decode( App\Helper::formatNumbersStats($total_campaigns) ) ?></span></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.campaigns')}}</h5>
				</div><!-- col-md-3 -->
				
				<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{ $settings->currency_symbol }}<?php echo html_entity_decode( App\Helper::formatNumbersStats($total_raised_funds) ) ?></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.funds_raised')}}</h5>
				</div><!-- col-md-3 -->

		</div><!-- row -->
		</div>
					
	</div><!-- container wrap-ui -->
	
	@else
	<div class="container margin-bottom-40">
		<div class="margin-bottom-30">
			<div class="btn-block text-center margin-top-40">
	    			<i class="ion ion-speakerphone ico-no-result"></i>
	    		</div>
	    		
	    		<h3 class="margin-top-none text-center no-result no-result-mg">
	    	{{ trans('misc.no_campaigns') }}
	    	</h3>
		</div>
	</div>
	@endif


	
	<div class="jumbotron jumbotron-bottom margin-bottom-zero jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
        <h1 class="title-site">{{trans('misc.title_cover_bottom')}}</h1>
        <p class="subtitle-site txt-center"><strong>{{$settings->welcome_subtitle}}</strong></p>

      </div><!-- container wrap-jumbotron -->
</div><!-- jumbotron -->
@endsection

@section('javascript')
	<script src="{{ asset('public/plugins/jquery.counterup/waypoints.min.js') }}"></script>
	<script src="{{ asset('public/plugins/jquery.counterup/jquery.counterup.min.js') }}"></script>
	
		<script type="text/javascript">
		
		$(document).on('click','#campaigns .loadPaginator', function(r){
			r.preventDefault();
			 $(this).remove();
			 $('.loadMoreSpin').remove();
					$('<div class="col-xs-12 loadMoreSpin"><a class="list-group-item text-center"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a></div>').appendTo( "#campaigns" );
					
					var page = $(this).attr('href').split('page=')[1];
					$.ajax({
						url: '{{ url("ajax/campaigns") }}?page=' + page
					}).done(function(data){
						if( data ) {
							$('.loadMoreSpin').remove();
							
							$( data ).appendTo( "#campaigns" );
						} else {
							bootbox.alert( "{{trans('misc.error')}}" );
						}
						//<**** - Tooltip
					});
			});
	
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
			delay: 10, // the delay time in ms
			time: 1000 // the speed time in ms
			});
		});
		
		 @if (session('success_verify'))
    		swal({   
    			title: "{{ trans('misc.welcome') }}",   
    			text: "{{ trans('users.account_validated') }}",   
    			type: "success",   
    			confirmButtonText: "{{ trans('users.ok') }}" 
    			});
   		 @endif
   		 
   		 @if (session('error_verify'))
    		swal({   
    			title: "{{ trans('misc.error_oops') }}",   
    			text: "{{ trans('users.code_not_valid') }}",   
    			type: "error",   
    			confirmButtonText: "{{ trans('users.ok') }}" 
    			});
   		 @endif
			$.fn.nextOrFirst = function(selector) {
    var next = this.next(selector);
    return (next.length) ? next : this.prevAll(selector).last();
}

$(function() {
    $('.fadein div:eq(0)').addClass("active");
    $('.fadein div:gt(0)').hide();
    setInterval(function() {
        $('.active:eq(0)').fadeOut().removeClass("active").nextOrFirst('div').addClass("active").fadeIn().end()
    }, 3000);
});
		</script>

@endsection